---
project_url: project-template4/project-template-java-maven
branch: main
---

# Project Template Java Maven

![Build Status](https://img.shields.io/gitlab/pipeline-status/project-template4/project-template-java-maven?branch=main&style=plastic)
![Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/project-template4/project-template-java-maven?branch=main)
![Test Result](https://img.shields.io/gitlab/pipeline-coverage/project-template4/project-template-java-maven?branch=main)
![Licence](https://img.shields.io/gitlab/license/project-template4/project-template-java-maven)
![Version](https://img.shields.io/gitlab/v/release/project-template4/project-template-java-maven?date_order_by=created_at&sort=date)
![Issue](https://img.shields.io/gitlab/issues/open-raw/project-template4/project-template-java-maven?gitlab_url=https%3A%2F%2Fgitlab.com)

# Nom Projet

## Description

## Prerequis

```txt
java 18
maven 5
```

## Installation

1. Etape 1
2. Clone the repo
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
3. Etape 2
4. Etape 3

## Usage

## Support

## Roadmap

- [x] Add Changelog
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
    - [ ] Chinese
    - [ ] Spanish

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a full list of proposed features (and known issues).

## Contribution

## Auteurs

* **Dev 1** - [@Roofne](https://github.com/Roofne) 

## License

## Status Projet

